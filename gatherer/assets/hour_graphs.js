(function() {
    var W = 200;
    var H = 25;

    var titleEls = document.getElementsByClassName("title");

    for(var i = 0; i < titleEls.length; i++) {
        var titleEl = titleEls[i];
        var hours = titleEl.getAttribute("data-hours")
            .split(" ")
            .filter(function(s) { return s != ""; })
            .map(function(s) { return parseInt(s, 10); });
        let max = Math.max.apply(Math, hours);

        var svg = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="';
        svg += [0, 0, W, H].join(" ");
        svg += '" preserveAspectRatio="none" width="100%" height="100%">';
        svg += '<path d="M 0 '+H;
        var x0 = 0;
        var y0 = H * (0.05 + 0.95 * (1 - hours[0] / max));
        svg += ' L 0,' + y0;
        for(var j = 1; j < hours.length; j++) {
            var x = W * j / (hours.length - 1);
            var y = H * (0.05 + 0.95 * (1 - hours[j] / max));
            var cx = (x0 + x) / 2;
            svg += ' C ' + cx + ',' + y0 + ' ' + cx + ',' + y + ' ' + x + ',' + y;
            x0 = x;
            y0 = y;
        }
        svg += ' L '+W+','+H+' z" stroke="#5582E0" stroke-width="1.5" stroke-linejoin="round" fill="#5582E0"/>';
        svg += '</svg>';

        var graphContainer = document.createElement("p");
        graphContainer.className = "graph";
        graphContainer.style.background = "url('data:image/svg+xml," + encodeURIComponent(svg) + "') no-repeat 100% 100%";
        titleEl.insertBefore(graphContainer, titleEl.getElementsByTagName("h3")[0]);
    }
})()
