(function() {
    var enableImages = !! localStorage.getItem("images_on");

    function startImages() {
	enableImages = true;
	var articleEls = document.getElementsByTagName("article");
	for(var i = 0; i < articleEls.length; i++) {
            var articleEl = articleEls[i];
            (function(articleEl) {
		var images = articleEl.getAttribute("data-images").split(" ").filter(function(image) { return !!image; });
		if (images.length > 0) {
                    var imageContainer = document.createElement("div");
                    imageContainer.className = "images";
                    if (images.length == 1) {
			/* Show a single image */
			var img = document.createElement("p");
			img.className = "image";
			img.style.backgroundImage = "url('" + images[0] + "')";
			imageContainer.appendChild(img);
                    } else {
			/* Show slideshow */
			/* url */
			var currentImage = null;
			/* <img> element */
			var currentImg = null;
			function show() {
			    if (!enableImages) {
				/* stop the recursion if control just disabled images */
				return;
			    }

                            var oldImage = currentImage;
                            var oldImg = currentImg;
                            var nextImage = null;
                            while(!currentImage || currentImage == oldImage) {
				currentImage = images[Math.floor(images.length * Math.random())];
                            }
                            var retryTimeout;
                            let image = new Image();
                            image.src = currentImage;
                            image.onload = function() {
				if (retryTimeout) {
                                    clearTimeout(retryTimeout);
				}

				currentImg = document.createElement("p");
				currentImg.className = "image";
				currentImg.alt = "Preview image from dubious source";
				currentImg.style.backgroundImage = "url('" + currentImage + "')";
				currentImg.style.left = oldImg ? "100%" : "0";
				imageContainer.appendChild(currentImg);

				requestAnimationFrame(function() {
                                    setTimeout(function() {
					/* slide */
					if (oldImg) {
                                            oldImg.style.left = "-100%";
					}
					currentImg.style.left = "0";

					setTimeout(function() {
                                            if (oldImg) {
						imageContainer.removeChild(oldImg);
                                            }
                                            /* loop */
                                            show();
					}, 5000 + Math.ceil(10000 * Math.random()));
                                    }, 100);
				});
                            };

                            retryTimeout = setTimeout(function() {
				/* img didn't load, cancel and retry */
				image.onload = null;
				show();
                            }, 5000);
			}
			show()
                    }

                    articleEl.style.backgroundPosition = "center";
                    articleEl.style.backgroundSize = "cover";
                    articleEl.className = "with-image";

                    var titleEl = articleEl.getElementsByClassName("title")[0];
                    if (titleEl && enableImages) {
			titleEl.insertBefore(imageContainer, titleEl.firstChild);
                    }
		}
            })(articleEl);
	}
    }

    var controlForm = document.createElement("form");
    var controlFormLabel = document.createElement("label");
    controlFormLabel.innerText = "Safety and Privacy:";
    controlForm.appendChild(controlFormLabel);
    function addInputToControlForm(id, text, title) {
	var input = document.createElement("input");
	input.setAttribute("id", id);
	input.setAttribute("name", "image_control");
	input.setAttribute("type", "radio");
	input.setAttribute("title", title);
	controlForm.appendChild(input);
	var label = document.createElement("label");
	label.innerText = text;
	label.setAttribute("for", id);
	label.setAttribute("title", title);
	controlForm.appendChild(label);
	return {
	    get: function() {
		return input.checked;
	    },
	    set: function() {
		input.setAttribute("checked", true);
	    },
	    onChange: function(cb) {
		input.addEventListener("change", cb);
	    },
	};
    }
    var controlImagesOff = addInputToControlForm(
	"images_off", "On",
	"Keep #FediBuzz a plain text website."
    );
    var controlImagesOn = addInputToControlForm(
	"images_on", "Off",
	"Load preview images from random servers, wildly depicting offensive indecencies. You have been warned!"
    );
    function onImageControlChange() {
	var state = controlImagesOn.get();
	if (state) {
	    localStorage.setItem("images_on", true);
	    startImages();
	} else {
	    localStorage.removeItem("images_on");
	    /* Remove images */
	    var imageContainers = document.getElementsByClassName("images");
	    for(var i = imageContainers.length - 1; i >= 0; i--) {
		imageContainers[i].parentNode.removeChild(imageContainers[i]);
	    }
	    /* Disable slideshows */
	    enableImages = false;
	}
    }
    if (enableImages) {
	controlImagesOn.set();
	startImages();
    } else {
	controlImagesOff.set();
    }
    controlImagesOff.onChange(onImageControlChange);
    controlImagesOn.onChange(onImageControlChange);
    document.getElementsByTagName("nav")[0].appendChild(controlForm);
})()
