use axum::{
    Extension,
    extract,
    http::StatusCode,
    response::{Html, IntoResponse},
};
use cave::db::Database;
use crate::{
    http_server::ServerState,
    oauth,
};

async fn collect_token(
    db: Database,
    http_client: &reqwest::Client,
    host: &str,
    code: String,
) -> Result<(), String> {
    // try a few registered apps until one works
    for (client_id, client_secret) in db.get_apps(host).await
        .map_err(|e| format!("{e}"))?
    {
        let app = oauth::Application {
            client_id,
            client_secret,
        };
        match app.obtain_token(http_client, host, code.clone()).await {
            Ok(token) => {
                db.add_token(host, &app.client_id, &token).await
                    .expect("db.add_token");
                // success, done!
                return Ok(());
            }
            Err(e) => {
                tracing::error!("obtain_token for {}: {}", host, e);
                // app seems blocked, remove
                let _ = db.delete_app(host, &app.client_id).await;
            }
        }
    }

    Err(format!("No registered app found for instance {host}"))
}

#[derive(serde::Deserialize)]
pub struct OAuthCode {
    code: Option<String>,
    error: Option<String>,
}

pub async fn get_token_collect(
    Extension(ServerState { db, http_client, mut store, .. }): Extension<ServerState>,
    extract::Path(host): extract::Path<String>,
    extract::Query(OAuthCode { code, error }): extract::Query<OAuthCode>,
) -> impl IntoResponse {
    if let Some(code) = code {
        // Success case
        match collect_token(db, &http_client, &host, code.clone()).await {
            Ok(()) => {
                let _ = store.save_host(&host).await;
                (
                    StatusCode::SEE_OTHER,
                    [("location", "/token/thanks")]
                ).into_response()
            }
            Err(e) => {
                tracing::error!("{}", e);
                (
                    StatusCode::INTERNAL_SERVER_ERROR,
                    [("content-type", "text/plain")],
                    e
                ).into_response()
            }
        }
    } else if error == Some("invalid_scope".to_owned()) {
        // Pixelfed has different scope names, retry with these:

        // Assume existing app from token_donate
        let (client_id, client_secret) = db.get_apps(&host).await
            .expect("db.get_apps")
            .into_iter()
            .nth(0)
            .unwrap();
        let app = oauth::Application {
            client_id,
            client_secret,
        };
        let url = app.generate_auth_url(&host, "read");
        (
            StatusCode::SEE_OTHER,
            [("location", url)]
        ).into_response()
    } else {
        (
            StatusCode::BAD_REQUEST,
            [("content-type", "text/plain")],
            "Not an OAuth request"
        ).into_response()
    }
}

pub async fn get_token_thanks() -> impl IntoResponse {
    Html(&include_bytes!("../../templates/token_thanks.html")[..])
}
