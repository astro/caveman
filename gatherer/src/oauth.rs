#[derive(serde::Serialize)]
struct AppRegistration {
    client_name: String,
    redirect_uris: String,
    scopes: String,
    website: String,
}

#[derive(serde::Serialize)]
struct TokenRequest {
    grant_type: String,
    scope: String,
    client_id: String,
    client_secret: String,
    redirect_uri: String,
    code: String,
}


#[derive(serde::Deserialize)]
struct TokenResult {
    pub access_token: String,
}

#[derive(serde::Deserialize)]
pub struct Application {
    pub client_id: String,
    pub client_secret: String,
}

pub const SCOPES: &str = "read:statuses";

impl Application {
    pub async fn register(client: &reqwest::Client, host: &str) -> Result<Self, reqwest::Error> {
        let url = format!("https://{host}/api/v1/apps");
        let form = AppRegistration {
            client_name: "#FediBuzz".to_string(),
            website: "https://fedi.buzz/".to_string(),
            redirect_uris: Self::generate_redirect_url(host),
            scopes: SCOPES.to_string(),
        };
        let res = client.post(url)
            .form(&form)
            .send()
            .await?
            .json()
            .await?;
        Ok(res)
    }

    pub fn generate_redirect_url(host: &str) -> String {
        format!("https://fedi.buzz/token/collect/{host}")
    }

    pub fn generate_auth_url(&self, host: &str, scopes: &str) -> String {
        format!(
            "https://{}/oauth/authorize?client_id={}&scope={}&response_type=code&redirect_uri={}",
            host,
            urlencoding::encode(&self.client_id),
            urlencoding::encode(scopes),
            urlencoding::encode(&Self::generate_redirect_url(host)),
        )
    }

    pub async fn obtain_token(&self, client: &reqwest::Client, host: &str, code: String) -> Result<String, reqwest::Error> {
        let url = format!("https://{host}/oauth/token");
        let form = TokenRequest {
            grant_type: "authorization_code".to_string(),
            scope: SCOPES.to_string(),
            client_id: self.client_id.clone(),
            client_secret: self.client_secret.clone(),
            redirect_uri: Self::generate_redirect_url(host),
            code,
        };
        let res: TokenResult = client.post(url)
            .form(&form)
            .send()
            .await?
            .json()
            .await?;
        Ok(res.access_token)
    }
}
