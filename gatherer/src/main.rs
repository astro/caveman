use std::time::Duration;
use metrics_util::MetricKindMask;
use metrics_exporter_prometheus::PrometheusBuilder;

use cave::{
    config::LoadConfig,
    firehose::FirehoseFactory,
};

mod config;
mod trends;
mod html_template;
mod http_server;
mod oauth;

#[tokio::main]
async fn main() {
    cave::init::exit_on_panic();
    cave::init::init_logger(5556);

    let config = config::Config::load();

    let recorder = PrometheusBuilder::new()
        .add_global_label("application", env!("CARGO_PKG_NAME"))
        .idle_timeout(MetricKindMask::ALL, Some(Duration::from_secs(600)))
        .install_recorder()
        .unwrap();

    let http_client = reqwest::Client::builder()
        .timeout(Duration::from_secs(10))
        .pool_max_idle_per_host(0)
        .user_agent(
            format!("{}/{} (+https://fedi.buzz/)", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"))
        )
        .build()
        .expect("reqwest::Client");

    cave::systemd::status("Connecting to database");
    let db = cave::db::Database::connect(&config.database).await;
    cave::systemd::status("Starting redis client");
    let store = cave::store::Store::new(
        8, config.redis.clone(), config.redis_password_file.clone()
    ).await;
    let cache = cave::cache::Cache::new(
        16, config.cache_redis.clone(), config.cache_redis_password_file.clone()
    ).await;

    let firehose_factory = FirehoseFactory::new(config.redis, config.redis_password_file);

    let http = http_server::start(
        config.listen_port,
        store,
        cache,
        db,
        http_client,
        firehose_factory,
        recorder,
    );
    cave::systemd::ready();
    http.await;
}
