use serde::Deserialize;
use sigh::{PrivateKey, Key};
use url::Url;

#[derive(Debug, Deserialize)]
pub struct Config {
    pub redis: String,
    pub redis_password_file: String,
    pub in_topic: String,
    priv_key_file: String,
    pub prometheus_address: String,
    pub prometheus_port: u16,
    pub blocklist: String,
}

impl Config {
    pub fn redis_url(&self) -> Url {
        let redis_password = std::fs::read_to_string(&self.redis_password_file)
            .expect("redis_password_file");
        let mut redis_url = Url::parse(&self.redis)
            .expect("redis_url");
        redis_url.set_password(Some(&redis_password)).unwrap();
        redis_url
    }

    pub fn priv_key(&self) -> PrivateKey {
        let data = std::fs::read_to_string(&self.priv_key_file)
            .expect("read priv_key_file");
        PrivateKey::from_pem(data.as_bytes())
            .expect("priv_key")
    }
}
