use futures::StreamExt;
use cave::{
    config::LoadConfig,
    feed::Post,
    firehose::FirehoseFactory,
};

mod config;

#[tokio::main]
async fn main() {
    cave::init::exit_on_panic();
    cave::init::init_logger(5555);

    let config = config::Config::load();
    let cache = cave::cache::Cache::new(
        16, config.cache_redis.clone(), config.cache_redis_password_file.clone()
    ).await;

    let firehose_factory = FirehoseFactory::new(config.redis, config.redis_password_file);
    let firehose = firehose_factory.produce()
        .await
        .expect("firehose");
    cave::systemd::ready();

    firehose.for_each(move |(event_type, data)| {
        if event_type != b"update" {
            // Only analyze new posts, no updates
            return futures::future::ready(());
        }

        let mut cache = cache.clone();
        tokio::spawn(async move {
            let post = match serde_json::from_slice::<Post>(&data) {
                Ok(post) => {
                    tracing::trace!("post {}", post.url);
                    post
                }
                Err(e) => {
                    tracing::error!("Cannot parse JSON: {:?}", e);
                    return;
                },
            };
            match cache.save_post(&data, &post).await {
                Ok(()) =>
                    cave::systemd::watchdog(),
                Err(e) =>
                    tracing::error!("Error saving post {}: {}", &post.url, e),
            }
        });

        futures::future::ready(())
    }).await;

    panic!("End")
}
