#[derive(Debug, serde::Deserialize)]
pub struct Config {
    pub redis: String,
    pub redis_password_file: String,
    pub cache_redis: String,
    pub cache_redis_password_file: String,
}
