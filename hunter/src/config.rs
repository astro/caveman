#[derive(Debug, serde::Deserialize)]
pub struct Config {
    pub redis: String,
    pub redis_password_file: String,
    pub database: String,
    pub hosts: Vec<String>,
    pub max_workers: usize,
    pub prometheus_address: String,
    pub prometheus_port: u16,
    pub blocklist: String,
}
