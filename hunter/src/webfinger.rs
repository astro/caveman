use std::{collections::HashSet, sync::Arc};

use cave::feed::url_host;
use crate::scheduler::Host;

#[derive(Debug, serde::Deserialize)]
struct Webfinger {
    #[serde(default = "Vec::new")]
    aliases: Vec<String>,
    #[serde(default = "Vec::new")]
    links: Vec<WebfingerLink>,
}

#[derive(Debug, serde::Deserialize)]
struct WebfingerLink {
    href: String,
}

pub async fn get_hosts_from_webfinger(client: &reqwest::Client, known_user: &str, host: &Host) -> Result<Vec<Host>, reqwest::Error> {
    let url = format!("https://{}/.well-known/webfinger?resource=acct:{}@{}", host, urlencoding::encode(known_user), urlencoding::encode(host));
    let webfinger: Webfinger = client.get(url)
        .send()
        .await?
        .json()
        .await?;
    let mut hosts = webfinger.aliases.iter()
        .filter_map(|url| url_host(&url[..]).map(Arc::new))
        .chain(
            webfinger.links.iter()
                .filter_map(|link| url_host(&link.href[..]).map(Arc::new))
        )
        .collect::<HashSet<_>>();
    hosts.remove(host.as_ref());
    Ok(hosts.into_iter().collect())
}
