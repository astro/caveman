{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }: {
    overlay = final: prev: {
      inherit (self.packages.${prev.stdenv.system}) caveman;
    };

    nixosModule = self.nixosModules.caveman;
    nixosModules.caveman = {
      imports = [ ./nixos-module.nix ];
      nixpkgs.overlays = [ self.overlay ];
    };

    nixosConfigurations.example = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        (nixpkgs + "/nixos/modules/virtualisation/qemu-vm.nix")
        {
          networking.hostName = "example";
          users.users.root.initialPassword = "";
          services.caveman.hunter = {
            enable = true;
            logLevel = "TRACE";
          };
          services.caveman.gatherer = {
            enable = true;
            logLevel = "TRACE";
          };
          virtualisation.forwardPorts = [ {
            # proto = "tcp";
            from = "host";
            # host.address = "0.0.0.0";
            host.port = 8000;
            # guest.address = "10.0.2.15";
            guest.port = 8000;
          } ];
          networking.firewall.allowedTCPPorts = [ 8000 ];
        }
        self.nixosModule
      ];
    };
  } //
  utils.lib.eachSystem (with utils.lib.system; [ x86_64-linux aarch64-linux ]) (system: let
    pkgs = nixpkgs.legacyPackages.${system};

    src = builtins.filterSource (path: type:
      builtins.match ".*\.nix" path == null
    ) ./.;

  in rec {
    packages.default = self.packages.${system}.caveman;

    packages.caveman = pkgs.rustPlatform.buildRustPackage rec {
      pname = "caveman";
      version = self.lastModifiedDate;
      inherit src;
      nativeBuildInputs = with pkgs; [ pkg-config ];
      buildInputs = with pkgs; [ openssl systemd ];
      cargoLock.lockFile = ./Cargo.lock;
      env.RUSTFLAGS = "--cfg tokio_unstable";
      postInstall = ''
        mkdir -p $out/share/caveman/gatherer
        cp -rv gatherer/{templates,assets} $out/share/caveman/gatherer/
      '';
    };

    # `nix develop`
    devShells.default = pkgs.mkShell {
      nativeBuildInputs = [
        pkgs.rust-analyzer
      ] ++
      (with packages.default; nativeBuildInputs ++ buildInputs);
    };
  });
}
