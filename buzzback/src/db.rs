use std::sync::Arc;
use tokio_postgres::{Client, Error, NoTls};


#[derive(Clone, Debug)]
pub struct Follow {
    /// Follower identification
    pub id: String,
    /// Subscribing inbox that is supposed to receive pushes
    pub inbox: String,
    /// Followed actor
    pub actor: String,
}

#[derive(Clone)]
pub struct Database {
    inner: Arc<DatabaseInner>,
}

struct DatabaseInner {
    client: Client,
}

impl Database {
    pub async fn connect(conn_str: &str) -> Self {
        let (client, connection) = tokio_postgres::connect(conn_str, NoTls)
            .await
            .unwrap();

        tokio::spawn(async move {
            if let Err(e) = connection.await {
                tracing::error!("postgresql: {}", e);
            }
        });

        Database {
            inner: Arc::new(DatabaseInner {
                client,
            }),
        }
    }

    pub async fn get_instance_followers(&self) -> Result<impl Iterator<Item = Follow>, Error> {
        let rows = self.inner.client.query("SELECT inbox, MIN(id), MIN(actor) FROM follows WHERE inbox LIKE '%/actor/inbox' GROUP BY inbox", &[])
            .await?;
        Ok(rows.into_iter()
           .map(|row| Follow {
               id: row.get(1),
               inbox: row.get(0),
               actor: row.get(2),
           })
        )
    }
}
