use std::sync::Arc;
use tokio_postgres::{Client, Error, NoTls, Statement};


const CREATE_SCHEMA_COMMANDS: &[&str] = &[
    "CREATE TABLE IF NOT EXISTS instance_apps (host TEXT NOT NULL, client_id TEXT NOT NULL, client_secret TEXT NOT NULL, UNIQUE (host, client_id))",
    "CREATE TABLE IF NOT EXISTS instance_tokens (host TEXT NOT NULL, client_id TEXT NOT NULL, token TEXT NOT NULL, created TIMESTAMP)",
    "CREATE INDEX IF NOT EXISTS instance_tokens_host ON instance_tokens (host, created)",
];

#[derive(Clone)]
pub struct Database {
    inner: Arc<DatabaseInner>,
}

struct DatabaseInner {
    client: Client,
    add_app: Statement,
    get_apps: Statement,
    delete_app: Statement,
    add_token: Statement,
    get_token: Statement,
    delete_token: Statement,
}

impl Database {
    pub async fn connect(conn_str: &str) -> Self {
        let (client, connection) = tokio_postgres::connect(conn_str, NoTls)
            .await
            .unwrap();

        tokio::spawn(async move {
            if let Err(e) = connection.await {
                tracing::error!("postgresql: {}", e);
            }
        });

        for command in CREATE_SCHEMA_COMMANDS {
            client.execute(*command, &[]).await
                .unwrap();
        }

        let add_app = client.prepare("INSERT INTO instance_apps (host, client_id, client_secret) VALUES ($1, $2, $3)")
            .await
            .unwrap();
        let get_apps = client.prepare("SELECT client_id, client_secret FROM instance_apps WHERE host=$1 LIMIT 32")
            .await
            .unwrap();
        let delete_app = client.prepare("DELETE FROM instance_apps WHERE host=$1 AND client_id=$2")
            .await
            .unwrap();
        let add_token = client.prepare("INSERT INTO instance_tokens (host, client_id, token, created) VALUES ($1, $2, $3, NOW())")
            .await
            .unwrap();
        let get_token = client.prepare("SELECT token FROM instance_tokens WHERE host=$1 ORDER BY RANDOM() LIMIT 1")
            .await
            .unwrap();
        let delete_token = client.prepare("DELETE FROM instance_tokens WHERE host=$1 AND token=$2")
            .await
            .unwrap();

        Database {
            inner: Arc::new(DatabaseInner {
                client,
                add_app,
                get_apps,
                delete_app,
                add_token,
                get_token,
                delete_token,
            }),
        }
    }

    pub async fn add_app(&self, host: &str, client_id: &str, client_secret: &str) -> Result<(), Error> {
        self.inner.client.execute(&self.inner.add_app, &[&host, &client_id, &client_secret])
            .await?;
        Ok(())
    }

    pub async fn get_apps(&self, host: &str) -> Result<Vec<(String, String)>, Error> {
        let rows = self.inner.client.query(&self.inner.get_apps, &[&host])
            .await?;
        Ok(rows.into_iter().filter_map(|row| {
            let client_id = row.try_get(0).ok()?;
            let client_token = row.try_get(1).ok()?;
            Some((client_id, client_token))
        }).collect())
    }

    pub async fn delete_app(&self, host: &str, client_id: &str) -> Result<(), Error> {
        self.inner.client.execute(&self.inner.delete_app, &[&host, &client_id])
            .await?;
        Ok(())
    }

    pub async fn add_token(&self, host: &str, client_id: &str, token: &str) -> Result<(), Error> {
        self.inner.client.execute(&self.inner.add_token, &[&host, &client_id, &token])
            .await?;
        Ok(())
    }

    pub async fn get_token(&self, host: &str) -> Result<Option<String>, Error> {
        let rows = self.inner.client.query(&self.inner.get_token, &[&host])
            .await?;
        Ok(rows.first().and_then(|row| row.try_get(0).ok()))
    }

    pub async fn delete_token(&self, host: &str, token: &str) -> Result<(), Error> {
        self.inner.client.execute(&self.inner.delete_token, &[&host, &token])
            .await?;
        Ok(())
    }
}
