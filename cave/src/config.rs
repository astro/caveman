pub trait LoadConfig {
    fn load() -> Self;
}

impl<T: Sized + for<'a> serde::Deserialize<'a>> LoadConfig for T {
    fn load() -> Self {
        let path = std::env::args().nth(1)
            .expect("Call with config.yaml");
        crate::systemd::status(&format!("Loading config file {path}"));

        let config_file = std::fs::read_to_string(path)
            .expect("read config");
        serde_yaml::from_str(&config_file)
            .expect("parse config")
    }
}
