use tracing::info;

pub fn status(text: &str) {
    info!("Status: {text}");
    systemd::daemon::notify(false, [(systemd::daemon::STATE_STATUS, text)].iter())
        .unwrap();
}

pub fn extend_timeout(usec: u64) {
    systemd::daemon::notify(false, [(systemd::daemon::STATE_EXTEND_TIMEOUT_USEC, format!("{usec}"))].iter())
        .unwrap();
}

pub fn ready() {
    systemd::daemon::notify(false, [(systemd::daemon::STATE_READY, "1")].iter())
        .unwrap();
}

pub fn watchdog() {
    systemd::daemon::notify(false, [(systemd::daemon::STATE_WATCHDOG, "1")].iter())
        .unwrap();
}
