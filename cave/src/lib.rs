pub mod init;
pub mod systemd;
pub mod config;
pub mod feed;
pub mod store;
pub mod trend_tag;
pub mod firehose;
pub mod live_file;
pub mod word_list;
pub mod db;
pub mod posts_cache;
pub mod activitypub;
pub mod block_list;
pub mod cache;


pub const PERIODS: &[u64] = &[4, 24, 7 * 24];

/// compare the selected period against how many multiples of preceding periods
pub const PERIOD_COMPARE_WINDOW: u64 = 3;

#[must_use] pub fn current_hour() -> u64 {
    chrono::offset::Utc::now().timestamp() as u64 / 3600
}
