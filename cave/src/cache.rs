use std::time::SystemTime;

use redis::{aio::ConnectionLike, RedisError, Value};
use url::Url;

use crate::{feed::Post, store::RedisPool};

const POST_LIFETIME: u64 = 86400;
const POSTS_PER_TAG: usize = 100;
const TAG_INTEREST_EXPIRE: u64 = 86400;

#[derive(Clone)]
pub struct Cache {
    pool: bb8::Pool<RedisPool>,
}

impl ConnectionLike for Cache {
    fn req_packed_command<'a>(
        &'a mut self,
        cmd: &'a redis::Cmd
    ) -> redis::RedisFuture<'a, Value> {
        Box::pin(async move {
            let mut conn = self.pool.get().await.unwrap();
            conn.req_packed_command(cmd).await
        })
    }

    fn req_packed_commands<'a>(
        &'a mut self,
        cmd: &'a redis::Pipeline,
        offset: usize,
        count: usize
    ) -> redis::RedisFuture<'a, Vec<Value>> {
        Box::pin(async move {
            let mut conn = self.pool.get().await.unwrap();
            conn.req_packed_commands(cmd, offset, count).await
        })
    }

    fn get_db(&self) -> i64 {
        // wrong ;-)
        0
    }
}

impl Cache {
    pub async fn new(pool_max_size: u32, cache_redis_url: String, cache_redis_password_file: String) -> Self {
        crate::systemd::status("Starting cache_redis client");
        let redis_password = std::fs::read_to_string(cache_redis_password_file)
            .expect("cache_redis_password_file");
        let mut redis_url = Url::parse(&cache_redis_url)
            .expect("redis_url");
        redis_url.set_password(Some(&redis_password)).unwrap();

        let pool = bb8::Pool::builder()
            .max_size(pool_max_size)
            .build(RedisPool { redis_url })
            .await
            .unwrap();
        Self { pool }
    }

    pub async fn save_post(&mut self, post_binary: &'_ [u8], post: &Post) -> Result<(), RedisError> {
        let Some(time) = post.timestamp() else {
            tracing::warn!("Post without timestamp");
            return Ok(());
        };
        let time = time.timestamp()
            // Don't allow time travel into the future
            .min(SystemTime::now()
                 .duration_since(SystemTime::UNIX_EPOCH)
                 .expect("SystemTime")
                 .as_secs() as i64
            );

        let tags: Vec<_> = post.tags_set().into_keys().collect();
        if tags.len() == 0 {
            return Ok(());
        }

        let mut cmd = redis::pipe();
        for tag in &tags {
            cmd.exists(format!("i:{tag}"));
        }
        let interested = cmd.query_async::<Vec<u8>>(self)
            .await?;
        tracing::trace!("save_post interested: {:?}", interested);
        let interested = interested.into_iter()
            .map(|count| count > 0)
            .zip(&tags)
            .filter_map(|(interested, tag)| {
                if interested {
                    Some(tag)
                } else {
                    None
                }
            })
            .collect::<Vec<_>>();

        if interested.len() == 0 {
            tracing::trace!("Not interested in any of {} tags", tags.len());
            return Ok(());
        }

        tracing::trace!("Saving post {} at {:?} to {} tags", post.url, time, interested.len());
        let mut cmd = redis::pipe();
        for tag in &interested {
            let key = format!("t:{tag}");
            // Link the post from tag zsets
            cmd.zadd(&key, &post.url, time)
                .ignore()
                // And obtain size of the tag zset for trimming
                .zcard(&key);
        }
        // Add the actual post
        cmd.set_ex(
            format!("p:{}", post.url),
            post_binary,
            POST_LIFETIME
        ).ignore();
        let tag_sizes = cmd.query_async::<Vec<usize>>(self)
            .await?;
        let tag_sizes = tag_sizes.iter()
            .zip(interested);

        // Trim excessive posts
        let tags_to_trim = tag_sizes.filter_map(|(size, tag)| {
            if *size > POSTS_PER_TAG {
                Some((tag, *size - POSTS_PER_TAG))
            } else {
                None
            }
        }).collect::<Vec<_>>();
        if tags_to_trim.len() > 0 {
            let mut cmd = redis::pipe();
            for (tag, trim_size) in tags_to_trim {
                cmd.zpopmin(
                    format!("t:{tag}"),
                    trim_size as isize
                ).ignore();
            }
            cmd.query_async::<()>(self)
                .await?;
        }

        Ok(())
    }

    pub async fn get_posts(&mut self, tag: &str, limit: usize) -> Result<Vec<Vec<u8>>, RedisError> {
        let tag = tag.to_lowercase();
        let tag_key = format!("t:{tag}");
        let interest_key = format!("i:{tag}");

        let mut cmd = redis::pipe();
        // First check if keys preexisted
        cmd.exists(&tag_key)
            .exists(&interest_key)
            // Mark interest
            .set_ex(&interest_key, "1", TAG_INTEREST_EXPIRE)
            .ignore();
        let (tag_preexisted, interest_preexisted) =
            cmd.query_async::<(bool, bool)>(self)
            .await?;

        if tag_preexisted && ! interest_preexisted {
            // Interest expired expire but tag preexisted, so it
            // contains very outdated posts. Flush it:
            tracing::info!("Flush tag {}", tag);
            redis::Cmd::del(&tag_key)
                .query_async::<()>(self)
                .await?;
            return Ok(vec![]);
        } else if ! tag_preexisted {
            return Ok(vec![]);
        }

        // Obtain post ids
        let post_ids = redis::Cmd::zrevrange(&tag_key, 0, limit.saturating_sub(1) as isize)
            .query_async::<Vec<String>>(self)
            .await?;
        if post_ids.len() == 0 {
            return Ok(vec![]);
        }

        // Get post contents
        redis::Cmd::mget(
            post_ids.iter()
                .map(|post_id| format!("p:{post_id}"))
                .collect::<Vec<_>>()
        )
            .query_async::<Vec<Option<Vec<u8>>>>(self)
            .await
            .map(|result| result.into_iter().flatten().collect())
    }
}
