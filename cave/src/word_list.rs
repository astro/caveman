use std::{sync::Arc, collections::HashSet};

use tokio::{
    io::{BufReader, AsyncBufReadExt},
    sync::RwLock,
};

#[derive(Clone)]
pub struct WordList {
    list: Arc<RwLock<HashSet<String>>>,
}

impl WordList {
    pub async fn new(path: &str) -> WordList {
        let list = crate::live_file::load(path, |file| async move {
            let mut list = HashSet::new();
            let mut file = BufReader::new(file);
            let mut line = String::new();
            while (file.read_line(&mut line).await).is_ok() {
                if line.is_empty() {
                    break
                }

                list.insert(line.trim_end().to_string());

                line = String::new();
            }
            list
        }).await.unwrap();


        WordList { list }
    }

    pub async fn contains(&self, word: &str) -> bool {
        self.list.read().await
            .contains(word)
    }
}
