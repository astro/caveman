#! /usr/bin/env bash

if [ -n "$HOST" ]; then
    HOST="$1"
else
    HOST=fedi.buzz
fi

curl -s https://$HOST/api/v1/streaming/public | (
    IFS="
"
    BOLD=$(tput bold)
    NORMAL=$(tput sgr0)
    while read -r line; do
        if [[ "$line" =~ ^data:\ (.+)$ ]]; then
            NAME="$(echo "${BASH_REMATCH[1]}" | jq -r .account.display_name 2>/dev/null)"
            if [ -n "$NAME" ]; then
              TEXT="$(echo "${BASH_REMATCH[1]}" | jq -r .content | sed -e 's/<[^>]*>//g')"
              echo "<$BOLD$NAME$NORMAL> $TEXT"
            fi
        fi
    done
)

