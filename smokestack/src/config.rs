#[derive(Debug, serde::Deserialize)]
pub struct Config {
    pub redis: String,
    pub redis_password_file: String,
    pub listen_port: u16,
}
