{ config, lib, pkgs, ... }:

let
  cfg = config.services.caveman;

  blocklistPath = "/etc/caveman.blocklist";
  profanityPath = "/etc/caveman.profanity";

  dbUser = "caveman";
  dbPassword = "c";

  butcherConfigFile = builtins.toFile "butcher.yaml" (builtins.toJSON cfg.butcher.settings);
  cacherConfigFile = builtins.toFile "cacher.yaml" (builtins.toJSON cfg.cacher.settings);
  gathererConfigFile = builtins.toFile "gatherer.yaml" (builtins.toJSON cfg.gatherer.settings);
  hunterConfigFile = builtins.toFile "hunter.yaml" (builtins.toJSON cfg.hunter.settings);
  sieveConfigFile = builtins.toFile "sieve.yaml" (builtins.toJSON cfg.sieve.settings);
  smokestackConfigFile = builtins.toFile "smokestack.yaml" (builtins.toJSON cfg.smokestack.settings);

  limitNOFILE = 1000000;
in
{
  options.services.caveman = with lib; {
    enable = lib.mkEnableOption "caveman";
    openFirewall = lib.mkEnableOption "" // { description = "Whether to open the fireall for the metrics ports."; };

    redis.port = mkOption {
      type = types.int;
      default = 6379;
    };
    redis.maxmemory = mkOption {
      type = types.str;
      default = "8gb";
    };
    redis.maxmemory-samples = mkOption {
      type = types.int;
      default = 10;
    };
    redis.passwordFile = mkOption {
      type = types.nullOr types.path;
      default = null;
    };
    cacheRedis.port = mkOption {
      type = types.int;
      default = 6378;
    };
    cacheRedis.maxmemory = mkOption {
      type = types.str;
      default = "128mb";
    };
    cacheRedis.maxmemory-samples = mkOption {
      type = types.int;
      default = 5;
    };
    cacheRedis.passwordFile = mkOption {
      type = types.nullOr types.path;
      default = null;
    };

    hunter.enable = mkEnableOption "caveman hunter";

    hunter.settings = mkOption {
      type = (pkgs.formats.yaml {}).type;
    };

    hunter.logLevel = mkOption {
      type = types.enum [ "ERROR" "WARN" "INFO" "DEBUG" "TRACE" ];
      default = "DEBUG";
    };

    cacher.enable = mkEnableOption "caveman cacher";

    cacher.settings = mkOption {
      type = (pkgs.formats.yaml {}).type;
    };

    cacher.logLevel = mkOption {
      type = types.enum [ "ERROR" "WARN" "INFO" "DEBUG" "TRACE" ];
      default = "DEBUG";
    };

    butcher.enable = mkEnableOption "caveman butcher";

    butcher.settings = mkOption {
      type = (pkgs.formats.yaml {}).type;
    };

    butcher.logLevel = mkOption {
      type = types.enum [ "ERROR" "WARN" "INFO" "DEBUG" "TRACE" ];
      default = "DEBUG";
    };

    gatherer.enable = mkEnableOption "caveman gatherer";

    gatherer.settings = mkOption {
      type = (pkgs.formats.yaml {}).type;
    };

    gatherer.logLevel = mkOption {
      type = types.enum [ "ERROR" "WARN" "INFO" "DEBUG" "TRACE" ];
      default = "DEBUG";
    };

    sieve.enable = mkEnableOption "caveman sieve";

    sieve.settings = mkOption {
      type = (pkgs.formats.yaml {}).type;
    };

    sieve.logLevel = mkOption {
      type = types.enum [ "ERROR" "WARN" "INFO" "DEBUG" "TRACE" ];
      default = "DEBUG";
    };

    smokestack.enable = mkEnableOption "caveman smokestack";

    smokestack.settings = mkOption {
      type = (pkgs.formats.yaml {}).type;
    };

    smokestack.logLevel = mkOption {
      type = types.enum [ "ERROR" "WARN" "INFO" "DEBUG" "TRACE" ];
      default = "DEBUG";
    };
  };

  config = lib.mkIf cfg.enable {
    networking.firewall.allowedTCPPorts = lib.mkIf cfg.openFirewall (lib.mkMerge [
      (lib.mkIf cfg.hunter.enable cfg.hunter.settings.prometheus_port)
      (lib.mkIf cfg.sieve.enable cfg.sieve.settings.prometheus_port)
    ]);

    systemd.tmpfiles.rules = [
      "L ${profanityPath} - - - - ${./profanity.txt}"
    ];

    services.caveman = {
      butcher.settings = lib.mapAttrsRecursive (_: lib.mkDefault) ({
        redis = "redis://127.0.0.1:${toString cfg.redis.port}/";
        profanity = profanityPath;
      } // lib.optionalAttrs (cfg.redis.passwordFile != null) {
        redis_password_file = cfg.redis.passwordFile;
      });

      cacher.settings = lib.mapAttrsRecursive (_: lib.mkDefault) ({
        redis = "redis://127.0.0.1:${toString cfg.redis.port}/";
        cache_redis = "redis://127.0.0.1:${toString cfg.cacheRedis.port}/";
      } // lib.optionalAttrs (cfg.redis.passwordFile != null) {
        redis_password_file = cfg.redis.passwordFile;
      } // lib.optionalAttrs (cfg.cacheRedis.passwordFile != null) {
        cache_redis_password_file = cfg.cacheRedis.passwordFile;
      });

      gatherer.settings = lib.mapAttrsRecursive (_: lib.mkDefault) ({
        redis = "redis://127.0.0.1:${toString cfg.redis.port}/";
        cache_redis = "redis://127.0.0.1:${toString cfg.cacheRedis.port}/";
        database = "host=localhost user=${dbUser} password=${dbPassword} dbname=caveman";
        listen_port = 8000;
      } // lib.optionalAttrs (cfg.redis.passwordFile != null) {
        redis_password_file = cfg.redis.passwordFile;
      } // lib.optionalAttrs (cfg.cacheRedis.passwordFile != null) {
        cache_redis_password_file = cfg.cacheRedis.passwordFile;
      });

      hunter.settings = lib.mapAttrsRecursive (_: lib.mkDefault) ({
        redis = "redis://127.0.0.1:${toString cfg.redis.port}/";
        database = "host=localhost user=${dbUser} password=${dbPassword} dbname=caveman";
        hosts = [ "mastodon.social" ];
        max_workers = 16;
        prometheus_address = "0.0.0.0";
        prometheus_port = 9101;
        blocklist = blocklistPath;
      } // lib.optionalAttrs (cfg.redis.passwordFile != null) {
        redis_password_file = cfg.redis.passwordFile;
      });

      sieve.settings = lib.mapAttrsRecursive (_: lib.mkDefault) ({
        redis = "redis://127.0.0.1:${toString cfg.redis.port}/";
        in_topic = "relay-in";
        prometheus_address = "0.0.0.0";
        prometheus_port = 9102;
        blocklist = blocklistPath;
      } // lib.optionalAttrs (cfg.redis.passwordFile != null) {
        redis_password_file = cfg.redis.passwordFile;
      });

      smokestack.settings = lib.mapAttrsRecursive (_: lib.mkDefault) ({
        redis = "redis://127.0.0.1:${toString cfg.redis.port}/";
        listen_port = 23;
      } // lib.optionalAttrs (cfg.redis.passwordFile != null) {
        redis_password_file = cfg.redis.passwordFile;
      });
    };

    services.redis.servers = {
      caveman = {
        enable = true;
        port = cfg.redis.port;
        requirePassFile = cfg.redis.passwordFile;
        settings = {
          inherit (cfg.redis) maxmemory maxmemory-samples;
          maxmemory-policy = "allkeys-lru";
        };
      };
      cache = {
        enable = true;
        port = cfg.cacheRedis.port;
        requirePassFile = cfg.cacheRedis.passwordFile;
        settings = {
          inherit (cfg.cacheRedis) maxmemory maxmemory-samples;
          maxmemory-policy = "allkeys-lru";
        };
      };
    };
    services.postgresql = {
      enable = true;
      initialScript = pkgs.writeScript "initScript" ''
        CREATE ROLE ${dbUser} LOGIN PASSWORD '${dbPassword}';
        CREATE DATABASE caveman TEMPLATE template0 ENCODING UTF8;
        GRANT ALL PRIVILEGES ON DATABASE caveman TO ${dbUser};
        GRANT ALL PRIVILEGES ON DATABASE caveman TO collectd;
      '';
      ensureUsers = [ {
        name = "collectd";
      } ];
    };
    services.collectd.plugins.postgresql = ''
      <Query hosts_with_tokens>
        Statement "select count(distinct host) from instance_tokens;"
        <Result>
          Type gauge
          InstancePrefix "unique_tokens"
          ValuesFrom "count"
        </Result>
      </Query>
      <Query total_tokens>
        Statement "select count(*) from instance_tokens;"
        <Result>
          Type gauge
          InstancePrefix "total_tokens"
          ValuesFrom "count"
        </Result>
      </Query>

      <Database ${config.networking.hostName}>
        Param database "caveman"
        Query total_tokens
        Query hosts_with_tokens
      </Database>
    '';

    # redis restore can be slow
    systemd.services.redis-caveman.serviceConfig.TimeoutStartSec = "infinity";

    systemd.services.caveman-hunter = lib.mkIf cfg.hunter.enable {
      wantedBy = [ "multi-user.target" ];
      requires = [
        "redis-caveman.service"
        "blocklist-update.service"
      ];
      after = [
        "redis-caveman.service"
        "postgresql.service"
        "network-online.target"
      ];
      wants = [ "network-online.target" ];
      environment.RUST_LOG = "caveman=${cfg.hunter.logLevel}";
      serviceConfig = {
        ExecStart = "${pkgs.caveman}/bin/caveman-hunter ${hunterConfigFile}";
        Type = "notify";
        WatchdogSec = 600;
        Restart = "always";
        RestartSec = 30;
        DynamicUser = true;
        User = "caveman-hunter";
        ProtectSystem = "strict";
        ProtectHome = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        RestrictNamespaces = true;
        RestrictRealtime = true;
        LockPersonality = true;
        MemoryDenyWriteExecute = true;
        LimitNOFILE = limitNOFILE;
        LimitRSS = "4G";
        MemoryMax = "16G";
      };
    };

    systemd.services.caveman-butcher = lib.mkIf cfg.butcher.enable {
      wantedBy = [ "multi-user.target" ];
      requires = [ "redis-caveman.service" ];
      after = [ "redis-caveman.service" "network-online.target" ];
      wants = [ "network-online.target" ];
      environment.RUST_LOG = "caveman=${cfg.butcher.logLevel}";
      serviceConfig = {
        ExecStart = "${pkgs.caveman}/bin/caveman-butcher ${butcherConfigFile}";
        Type = "notify";
        WatchdogSec = 600;
        Restart = "always";
        RestartSec = 30;
        DynamicUser = true;
        User = "caveman-butcher";
        ProtectSystem = "strict";
        ProtectHome = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        RestrictNamespaces = true;
        RestrictRealtime = true;
        LockPersonality = true;
        MemoryDenyWriteExecute = true;
        LimitNOFILE = limitNOFILE;
        MemoryMax = "2G";
      };
    };

    systemd.services.caveman-cacher = lib.mkIf cfg.cacher.enable {
      wantedBy = [ "multi-user.target" ];
      requires = [ "redis-caveman.service" "redis-cache.service" ];
      after = [ "redis-caveman.service" "redis-cache.service" "network-online.target" ];
      wants = [ "network-online.target" ];
      environment.RUST_LOG = "caveman=${cfg.cacher.logLevel}";
      serviceConfig = {
        ExecStart = "${pkgs.caveman}/bin/caveman-cacher ${cacherConfigFile}";
        Type = "notify";
        WatchdogSec = 600;
        Restart = "always";
        RestartSec = 30;
        DynamicUser = true;
        User = "caveman-cacher";
        ProtectSystem = "strict";
        ProtectHome = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        RestrictNamespaces = true;
        RestrictRealtime = true;
        LockPersonality = true;
        MemoryDenyWriteExecute = true;
        LimitNOFILE = limitNOFILE;
        MemoryMax = "2G";
      };
    };

    systemd.services.caveman-gatherer = lib.mkIf cfg.gatherer.enable {
      wantedBy = [ "multi-user.target" ];
      requires = [ "redis-caveman.service" "redis-cache.service" ];
      after = [ "redis-caveman.service" "redis-cache.service" "postgresql.service" "network-online.target" ];
      wants = [ "network-online.target" ];
      environment.RUST_LOG = "caveman=${cfg.gatherer.logLevel}";
      serviceConfig = {
        ExecStart = "${pkgs.caveman}/bin/caveman-gatherer ${gathererConfigFile}";
        Type = "notify";
        WatchdogSec = 90;
        Restart = "always";
        RestartSec = 1;
        DynamicUser = true;
        User = "caveman-gatherer";
        ProtectSystem = "strict";
        ProtectHome = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        RestrictNamespaces = true;
        RestrictRealtime = true;
        LockPersonality = true;
        MemoryDenyWriteExecute = true;
        LimitNOFILE = limitNOFILE;
        WorkingDirectory = "${pkgs.caveman}/share/caveman/gatherer";
        MemoryMax = "1G";
      };
    };

    systemd.timers.caveman-gatherer-probe = lib.mkIf cfg.gatherer.enable {
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "minutely";
    };
    systemd.services.caveman-gatherer-probe = lib.mkIf cfg.gatherer.enable {
      requires = [ "caveman-gatherer.service" ];
      serviceConfig = {
        Type = "oneshot";
        User = "caveman-gatherer-probe";
        DynamicUser = true;
        LimitNOFILE = limitNOFILE;
        ProtectSystem = "full";
      };
      path = with pkgs; [ wget ];
      script = ''
        wget -O /dev/null --user-agent=caveman-gatherer-probe 127.0.0.1:${toString cfg.gatherer.settings.listen_port}/
      '';
    };

    systemd.services.caveman-sieve = lib.mkIf cfg.sieve.enable {
      wantedBy = [ "multi-user.target" ];
      requires = [ "redis-caveman.service" ];
      after = [ "redis-caveman.service" "network-online.target" ];
      wants = [ "network-online.target" ];
      environment.RUST_LOG = "caveman=${cfg.sieve.logLevel}";
      serviceConfig = {
        ExecStart = "${pkgs.caveman}/bin/caveman-sieve ${sieveConfigFile}";
        Type = "notify";
        WatchdogSec = 300;
        Restart = "always";
        RestartSec = 1;
        DynamicUser = true;
        User = "caveman-sieve";
        ProtectSystem = "strict";
        ProtectHome = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        RestrictNamespaces = true;
        RestrictRealtime = true;
        LockPersonality = true;
        MemoryDenyWriteExecute = true;
        LimitNOFILE = limitNOFILE;
        LimitRSS = "128M:256M";
      };
    };

    systemd.services.caveman-smokestack = lib.mkIf cfg.smokestack.enable {
      wantedBy = [ "multi-user.target" ];
      requires = [ "redis-caveman.service" "caveman-hunter.service" ];
      after = [ "redis-caveman.service" "caveman-hunter.service" "network-online.target" ];
      wants = [ "network-online.target" ];
      environment.RUST_LOG = "caveman=${cfg.smokestack.logLevel}";
      serviceConfig = {
        ExecStart = "${pkgs.caveman}/bin/caveman-smokestack ${smokestackConfigFile}";
        Type = "notify";
        WatchdogSec = 10;
        Restart = "always";
        RestartSec = 10;
        DynamicUser = true;
        User = "caveman-smokestack";
        ProtectSystem = "strict";
        ProtectHome = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        RestrictNamespaces = true;
        RestrictRealtime = true;
        LockPersonality = true;
        MemoryDenyWriteExecute = true;
        LimitNOFILE = limitNOFILE;
        LimitRSS = "64M:256M";
        # Allow listening on ports <1024
        AmbientCapabilities = "CAP_NET_BIND_SERVICE";
      };
    };

    systemd.services.blocklist-update = lib.mkIf cfg.hunter.enable {
      after = [ "network.target" "network-online.target" ];
      wants = [ "network-online.target" ];
      path = with pkgs; [ coreutils wget ];
      script = ''
        T=$(mktemp blocklistXXXX)
        wget -O $T https://raw.githubusercontent.com/gardenfence/blocklist/main/gardenfence.txt
        chmod a+r $T
        mv $T ${lib.escapeShellArg blocklistPath}
      '';
      serviceConfig = {
        Type = "oneshot";
        Restart = "on-failure";
        RestartSec = 600;
      };
    };
    systemd.timers.blocklist-update = lib.mkIf cfg.hunter.enable {
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
  };
}
